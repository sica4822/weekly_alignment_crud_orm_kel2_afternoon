const { supplier } = require("../models");

class Supplier {
  async getAllSupplier(req, res, next) {
    try {
      let data = await supplier.findAll();
      // If Supplier not exists
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Supplier not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async getDetailSupplier(req, res, next) {
    try {
      let data = await supplier.findOne({
        // find one data in supplier table
        where: { id: req.params.id },
      });

      if (!data) {
        return res.status(404).json({ errors: ["Data of supplier not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server error"] });
    }
  }

  async createSupplier(req, res, next) {
    try {
      // Create supplier
      const newData = await supplier.create(req.body);

      // Find supplier with join
      const data = await supplier.findOne({
        where: {
          id: newData.id,
        },
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateSupplier(req, res, next) {
    try {
      const updateDate = await supplier.update(req.body, {
        where: { id: req.params.id },
      });

      // If no data updated
      if (updateDate[0] === 0) {
        return res.status(404).json({ errors: ["Data of supplier not found"] });
      }

      // Find the updated supplier
      const data = await supplier.findOne({
        where: {
          id: req.params.id,
        },
      });

      // If success
      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteSupplier(req, res, next) {
    try {
      // Delete data
      let data = await supplier.destroy({ where: { id: req.params.id } });

      // If data deleted is null
      if (!data) {
        return res.status(404).json({ errors: ["Data of supplier is not found"] });
      }

      // If success
      res.status(200).json({ message: "Success delete data of supplier" });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Supplier();
