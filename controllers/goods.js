const { good, supplier } = require("../models");

class Good {
  async getAllGoods(req, res, next) {
    try {
      let data = await good.findAll({
        // find all data in goods table
        attributes: { exclude: ["id_supplier"] },
        include: [
          // include is join
          {
            model: supplier,
          },
        ],
      });
      // If goods not exists
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Goods not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async getDetailGood(req, res, next) {
    try {
      let data = await good.findOne({
        // find one data in goods table
        where: { id: req.params.id },
        attributes: { exclude: ["id_supplier"] },
        include: [
          // include is join
          {
            model: supplier,
          },
        ],
      });

      if (!data) {
        return res.status(404).json({ errors: ["Data of goods not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server error"] });
    }
  }

  async createGood(req, res, next) {
    try {
      // Create good
      const newData = await good.create(req.body);

      // Find good with join
      const data = await good.findOne({
        where: {
          id: newData.id,
        },
        attributes: { exclude: ["id_supplier"] },
        include: [
          {
            model: supplier,
          },
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateGood(req, res, next) {
    try {
      const updateDate = await good.update(req.body, {
        where: { id: req.params.id },
      });

      // If no data updated
      if (updateDate[0] === 0) {
        return res.status(404).json({ errors: ["Data of goods not found"] });
      }

      // Find the updated goods
      const data = await good.findOne({
        where: {
          id: req.params.id,
        },
        attributes: { exclude: ["id_supplier"] },
        include: [
          {
            model: supplier,
          },
        ],
      });

      // If success
      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteGood(req, res, next) {
    try {
      // Delete data
      let data = await good.destroy({ where: { id: req.params.id } });

      // If data deleted is null
      if (!data) {
        return res.status(404).json({ errors: ["Data of good is not found"] });
      }

      // If success
      res.status(200).json({ message: "Success delete data of goods" });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Good();
