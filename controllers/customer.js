const { customer } = require("../models");

class Customer {
  async getAllCustomer(req, res, next) {
    try {
      let data = await customer.findAll();
      // If customer not exists
      if (data.length === 0) {
        return res.status(404).json({ errors: ["customer not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async getDetailCustomer(req, res, next) {
    try {
      let data = await customer.findOne({
        // find one data in customer table
        where: { id: req.params.id },
      });

      if (!data) {
        return res.status(404).json({ errors: ["Data of customer not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server error"] });
    }
  }

  async createCustomer(req, res, next) {
    try {
      // Create customer
      const newData = await customer.create(req.body);

      // Find customer with join
      const data = await customer.findOne({
        where: {
          id: newData.id,
        },
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateCustomer(req, res, next) {
    try {
      const updateDate = await customer.update(req.body, {
        where: { id: req.params.id },
      });

      // If no data updated
      if (updateDate[0] === 0) {
        return res.status(404).json({ errors: ["Data of customer not found"] });
      }

      // Find the updated customer
      const data = await customer.findOne({
        where: {
          id: req.params.id,
        },
      });

      // If success
      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteCustomer(req, res, next) {
    try {
      // Delete data
      let data = await customer.destroy({ where: { id: req.params.id } });

      // If data deleted is null
      if (!data) {
        return res
          .status(404)
          .json({ errors: ["Data of customer is not found"] });
      }

      // If success
      res.status(200).json({ message: "Success delete data of customer" });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Customer();
