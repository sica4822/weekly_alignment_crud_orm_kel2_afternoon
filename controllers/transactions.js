const { transaction, good, customer, supplier } = require("../models"); // Import connection

class Transaction {
  // Get all transaction
  async getAllTransactions(req, res) {
    try {
      // Find all data in transactions
      // const data = await transaction.findAll();
      // {
      //   attributes: ["id", "quantity", "total"], //Untuk menampilkan atribut yang ingin ditampilkan
      // }

      let data = await transaction.findAll({
        // find all data in transactions table
        attributes: { exclude: ["id_good", "id_customer"] },
        include: [
          // include is join
          {
            model: customer,
          },
          {
            model: good,
            include: [
              {
                model: supplier,
              },
            ],
          },
        ],
      });

      // If transactions not exists
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Transactions not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get detail transaction
  async getDetailTransaction(req, res, next) {
    try {
      // // Find all data in transactions
      // const data = await transaction.findOne({
      //   where: { id: req.params.id },
      // });

      let data = await transaction.findOne({
        // find one data in transactions table
        where: { id: req.params.id },
        attributes: { exclude: ["id_good", "id_customer"] },
        include: [
          // include is join
          {
            model: customer,
          },
          {
            model: good,
            include: [
              {
                model: supplier,
              },
            ],
          },
        ],
      });

      if (!data) {
        return res.status(404).json({ errors: ["Transaction not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Create transaction
  async createTransaction(req, res, next) {
    try {
      // create data transcation
      console.log(req.body);
      const insertData = await transaction.create(req.body);

      const data = await transaction.findOne({
        where: { id: insertData.id },
        include: [
          { model: customer },
          { model: good, include: [{ model: supplier }] },
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal server error"] });
    }

    // try {
    //   const dataGood = await good.findOne({ where: { id: req.body.id_good } });
    //   const price = dataGood.dataValues.price;
    //   const total = parseFloat(price) * parseFloat(req.body.quantity);

    //   // Insert Data
    //   const data = await transaction.create({
    //     id_good: req.body.id_good,
    //     id_customer: req.body.id_customer,
    //     quantity: req.body.quantity,
    //     total,
    //   });

    //   res.status(201).json(data);
    // } catch (error) {
    //   res.status(500).json({ errors: ["Internal Server Error"] });
    // }
  }

  async updateTransaction(req, res, next) {
    try {
      const updateDate = await transaction.update(req.body, {
        where: { id: req.params.id },
      });

      // If no data updated
      if (updateDate[0] === 0) {
        return res.status(404).json({ errors: ["Transaction not found"] });
      }

      // Find the updated transaction
      const data = await transaction.findOne({
        where: {
          id: req.params.id,
        },
        attributes: { exclude: ["id_good", "id_customer"] },
        include: [
          {
            model: customer,
          },
          {
            model: good,
            include: [
              {
                model: supplier,
              },
            ],
          },
        ],
      });

      // If success
      res.status(201).json({ data });

      // // Find good and price
      // const dataGood = await good.findOne({ where: { id: req.body.id_good } });

      // if (!dataGood) {
      //   return res.status(404).json({ errors: ["Good not found"] });
      // }

      // const dataCustomer = await customer.findOne({
      //   where: { id: req.body.id_customer },
      // });

      // if (!dataCustomer) {
      //   return res.status(404).json({ errors: ["Customer not found"] });
      // }

      // const price = dataGood.dataValues.price;
      // const total = parseFloat(price) * parseFloat(req.body.quantity);
      // // Update Data
      // const updateData = await transaction.update(
      //   {
      //     id_good: req.body.id_good,
      //     id_customer: req.body.id_customer,
      //     quantity: req.body.quantity,
      //     total,
      //   },
      //   {
      //     where: { id: req.params.id },
      //   }
      // );

      // const updatedData = await transaction.findOne({
      //   where: { id: req.params.id },
      // });

      // if (!updatedData) {
      //   return res.status(404).json({ errors: ["Transaction not found"] });
      // }

      // res.status(200).json({ updatedData });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteTransaction(req, res, next) {
    try {
      // const deletedData = await transaction.destroy({
      //   where: { id: req.params.id },
      // });

      // if (!deletedData) {
      //   return res
      //     .status(404)
      //     .json({ errors: ["Transaction not found or it is not exists"] });
      // }

      // res.status(200).json({ data: {} });

      // Delete data
      let data = await transaction.destroy({ where: { id: req.params.id } });

      // If data deleted is null
      if (!data) {
        return res.status(404).json({ errors: ["Transaction not found"] });
      }

      // If success
      res.status(200).json({ message: "Success delete transaction" });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Transaction();
