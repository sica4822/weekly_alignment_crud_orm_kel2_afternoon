const path = require("path");
const crypto = require("crypto");
const validator = require("validator");
const { supplier } = require("../../models");
const { promisify } = require("util");

exports.createOrUpdateGoodValidator = async (req, res, next) => {
  try {
    const errors = [];

    // Check input of the name
    if (validator.isEmpty(req.body.name, { ignore_whitespace: false })) {
      errors.push("please input the name");
    }

    // Check input of price
    if (validator.isEmpty(req.body.price, { ignore_whitespace: false })) {
      errors.push("please input the price");
    } else if (!validator.isNumeric(req.body.price)) {
      // Check price is a number
      errors.push("price must be number");
    }

    // Find id supplier
    const findSupplier = await supplier.findOne({
      where: { id: req.body.id_supplier },
    });

    // Check input of supplier
    if (validator.isEmpty(req.body.id_supplier, { ignore_whitespace: false })) {
      errors.push("please input the id supplier");
    } else if (!validator.isInt(req.body.id_supplier)) {
      errors.push("id_supplier must be number (integer)");
    } else if (!findSupplier) {
      errors.push("Supplier is not found");
    }

    // Check for the image was upload or not

    if (!(req.files && req.files.image)) {
      errors.push("Please upload the image");
    } else if (req.files.image) {
      // If image was uploaded

      // req.files.image is come from key (file) in postman
      const file = req.files.image;

      // Make sure image is photo
      if (!file.mimetype.startsWith("image")) {
        errors.push("File must be an image");
      }

      // Check file size (max 1MB)
      if (file.size > 2000000) {
        errors.push("Image must be less than 2MB");
      }

      // If error
      if (errors.length > 0) {
        return res.status(400).json({ errors: errors });
      }

      // Create custom filename
      let fileName = crypto.randomBytes(16).toString("hex");

      // Rename the file
      file.name = `${fileName}${path.parse(file.name).ext}`;

      // Make file.mv to promise
      const move = promisify(file.mv);

      // Upload image to /public/images
      await move(`./public/images/goods/${file.name}`);

      // assign req.body.image with file.name
      req.body.image = file.name;
    }

    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ errors: ["Internal Server Error"] });
  }
};
