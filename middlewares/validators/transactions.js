const validator = require("validator");
const { good, customer } = require("../../models");

exports.createOrUpadateTransactionValidator = async (req, res, next) => {
  try {
    // Variabel to save errors
    const errors = [];

    // Checkt all input from user
    if (!validator.isInt(req.body.id_good)) {
      errors.push(`Id Good must be a number`);
    }

    if (!validator.isInt(req.body.id_customer)) {
      errors.push(`Id Customer must be a number`);
    }

    if (!validator.isInt(req.body.quantity)) {
      errors.push(`Quantity must be a number`);
    }

    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }

    // Find good, price, and total
    const findGood = await good.findOne({ where: { id: req.body.id_good } });

    if (!findGood) {
      errors.push("Good is not found");
    }

    // Find Customer
    const findCustomer = await customer.findOne({
      where: { id: req.body.id_customer },
    });

    if (!findCustomer) {
      errors.push("Customer is not found");
    }

    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }

    const { price } = findGood;
    req.body.total = parseFloat(req.body.quantity) * parseFloat(price);

    next();
  } catch (error) {
    return res.status(400).json({ errors: ["Bad request"] });
  }
};
