const express = require("express");

// Import validator
const {
  createOrUpdateCustomerValidator,
} = require("../middlewares/validators/customer");

// Import controller
const {
  getAllCustomer,
  getDetailCustomer,
  createCustomer,
  updateCustomer,
  deleteCustomer,
} = require("../controllers/customer");

const router = express.Router();

router
  .route("/")
  .get(getAllCustomer)
  .post(createOrUpdateCustomerValidator, createCustomer);

router
  .route("/:id")
  .get(getDetailCustomer)
  .put(createOrUpdateCustomerValidator, updateCustomer)
  .delete(deleteCustomer);

module.exports = router;
