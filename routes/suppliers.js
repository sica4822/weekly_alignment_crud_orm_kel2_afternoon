const express = require("express");

// Import validator
const {
  createOrUpdateSupplierValidator,
} = require("../middlewares/validators/suppliers");

// Import controller
const {
  getAllSupplier,
  getDetailSupplier,
  createSupplier,
  updateSupplier,
  deleteSupplier,
} = require("../controllers/suppliers");

const router = express.Router();

router
  .route("/")
  .get(getAllSupplier)
  .post(createOrUpdateSupplierValidator, createSupplier);

router
  .route("/:id")
  .get(getDetailSupplier)
  .put(createOrUpdateSupplierValidator, updateSupplier)
  .delete(deleteSupplier);

module.exports = router;
