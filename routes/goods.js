const express = require("express");

// Import validator
const {
  createOrUpdateGoodValidator,
} = require("../middlewares/validators/goods");

// Import controller
const {
  getAllGoods,
  getDetailGood,
  createGood,
  updateGood,
  deleteGood,
} = require("../controllers/goods");

const router = express.Router();

router
  .route("/")
  .get(getAllGoods)
  .post(createOrUpdateGoodValidator, createGood);

router
  .route("/:id")
  .get(getDetailGood)
  .put(createOrUpdateGoodValidator, updateGood)
  .delete(deleteGood);

module.exports = router;
