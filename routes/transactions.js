const express = require("express"); // Import express

// Import validator
const {
  createOrUpadateTransactionValidator,
} = require("../middlewares/validators/transactions");

// Import controller
const {
  getAllTransactions,
  getDetailTransaction,
  createTransaction,
  updateTransaction,
  deleteTransaction,
} = require("../controllers/transactions");

// Make router
const router = express.Router();

router.get("/", getAllTransactions);
router.get("/:id", getDetailTransaction);
router.post("/", createOrUpadateTransactionValidator, createTransaction);
router.put("/:id", updateTransaction);
router.delete("/:id", deleteTransaction);

module.exports = router; // Export router
